/* Author: Gopal Rathore

*/

$(document).ready(function(){

	var tab = ['branding', 'web', 'logo', 'photography'];

// Flexslider code
$('.flexslider').flexslider({
	animation: "slide",
	controlsContainer: $(".custom-controls-container"),
	customDirectionNav: $(".custom-navigation a")
});

	// Tabs in works section

	$('.tabs li').click(function(){
		$('.list-item li').hide();
		$('.tabs li').removeClass('tab-active');
		$(this).addClass('tab-active');
		var ul = $(this).parent();
		var index = ul.children().index(this);

		if(index==0){
			$('.list-item li').show();
		}else{
			$('.'+tab[index-1]).show();
		}

	});

	// setting the height of banner
	var height = window.innerHeight;
	$('.banner').css('height', height);
	$('.banner img').css('height', height);

});




















